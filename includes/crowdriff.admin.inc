<?php

/**
 * @file
 * CrowdRiff module admin pages.
 */

require_once __DIR__ . '/crowdriff.api.inc';

/**
 * CrowdRiff administration settings form.
 */
function crowdriff_admin_settings($form, &$form_state) {

  $form['crowdriff_api_key'] = [
    '#type' => 'textfield',
    '#title' => 'CrowdRiff Drupal Key',
    '#default_value' => variable_get('crowdriff_api_key', ''),
  ];

  return system_settings_form($form);
}

/**
 * CrowdRiff admin settings form validation callback.
 */
function crowdriff_admin_settings_validate($form, &$form_state) {

  if ($form_state['values']['crowdriff_api_key'] == '') {
    return;
  }

  // Liberally match against UUID pattern.
  if (!preg_match('/^[0-9a-f]{8}(-[0-9a-f]{4}){3}-[0-9a-f]{12}$/i', $form_state['values']['crowdriff_api_key'])) {
    form_set_error('crowdriff_api_key', t('Invalid CrowdRiff Drupal Key'));
  }
  else {
    // Do a test query to the API.
    // @see crowdriff_api_request().
    $query = drupal_http_build_query([
      'token' => $form_state['values']['crowdriff_api_key'],
    ]);
    $response = drupal_http_request(CROWDRIFF_API_BASE_URL . 'folders' . '?' . $query);
    if ($response->code != 200) {
      form_set_error('crowdriff_api_key', t('A request to CrowdRiff with the provided Drupal Key failed.'));
    }
  }
}
