<?php

/**
 * @file
 * Functions for retrieving information from the CrowdRiff API.
 */

define('CROWDRIFF_API_BASE_URL', 'https://api.crowdriff.com/v1/');

/**
 * Make a request to the CrowdRiff API.
 *
 * @param string $path
 *   The resource path.
 * @param array $params
 *   Request parameters.
 *
 * @return array|object|false
 *   The response data, either as an object or array of objects, or FALSE if an
 *   error occured.
 */
function crowdriff_api_request($path, array $params = []) {

  ksort($params);
  $cidComponents = array_map(
    function ($value, $key) {
      return $key . '-' . $value;
    },
    $params, array_keys($params)
  );
  array_unshift($cidComponents, $path);
  $cid = implode(':', $cidComponents);

  $cache = cache_get($cid, 'cache_crowdriff');
  if (!empty($cache) && $cache->expire >= REQUEST_TIME) {
    return $cache->data;
  }

  $apiKey = variable_get('crowdriff_api_key');
  if (empty($apiKey)) {
    watchdog('crowdriff', 'CrowdRiff request made without API key configured.', [], WATCHDOG_ERROR);
    return FALSE;
  }

  $params += [
    'token' => $apiKey,
  ];
  $query = drupal_http_build_query($params);
  $response = drupal_http_request(CROWDRIFF_API_BASE_URL . $path . '?' . $query);
  $responseBody = json_decode($response->data);

  if ($response->code != 200) {
    watchdog('crowdriff', 'CrowdRiff error: %status', ['%status' => $responseBody->error], WATCHDOG_ERROR);
    return FALSE;
  }

  if (!empty($response->headers['cache-control'])) {
    preg_match('/max-age=(\d+)/', $response->headers['cache-control'], $ageMatch);
    cache_set($cid, $responseBody->data, 'cache_crowdriff', REQUEST_TIME + intval($ageMatch[1]));
  }

  return $responseBody->data;
}

/**
 * Check the 'include' parameter for a valid value.
 *
 * @param string $value
 *   The include parameter value.
 *
 * @return string|null
 *   The include parameter value, or null if an invalid value was provided.
 *
 * @todo Support multiple values.
 *   This functionality is indicated in the API docs, but examples of the
 *   correct format are not provided.
 */
function _crowdriff_api_check_asset_include($value) {
  $allowedValues = [
    'catalysts',
    'tags',
  ];

  if (!in_array($value, $allowedValues)) {
    watchdog(
      'crowdriff',
      'Unknown asset include provided: %include',
      [
        '%include' => $value,
      ],
      WATCHDOG_NOTICE
    );
    return NULL;
  }

  return $value;
}

/**
 * Check filters for an asset query.
 *
 * @param array $filter
 *   An array of filter parameters.
 *
 * @return array
 *   The provided array of filters with unknown keys removed.
 */
function _crowdriff_api_check_asset_filters(array $filter) {
  $allowedFilters = array_fill_keys(
    ['tag', 'lens', 'size', 'offset', 'since'],
    TRUE
  );

  $unknownFilters = array_diff_key($filter, $allowedFilters);
  if (!empty($unknownFilters)) {
    watchdog(
      'crowdriff',
      'Unknown asset filter provided: %unknown_filters',
      [
        '%unknown_filters' => implode(',', $unknownFilters),
      ],
      WATCHDOG_NOTICE
    );
  }

  return array_intersect_key($filter, $allowedFilters);
}

/**
 * Retrieve a single asset.
 *
 * @param string $uuid
 *   The asset UUID.
 * @param string $include
 *   Include the asset's associated tags and/or catalysts in the response body.
 *
 * @return object|false
 *   An asset object, or FALSE if an error occurred.
 */
function crowdriff_api_get_asset($uuid, $include = NULL) {
  $params = [];

  if (($include = _crowdriff_api_check_asset_include($include))) {
    $params['include'] = $include;
  }

  return crowdriff_api_request('assets/' . $uuid, $params);
}

/**
 * Retrieve a set of assets.
 *
 * @param array $filter
 *   Additional options to filter results by.
 * @param string $include
 *   Include the asset's associated tags and/or catalysts in the response body.
 *
 * @return array|false
 *   An array of asset objects, or FALSE if an error occurred.
 */
function crowdriff_api_list_assets(array $filter = [], $include = NULL) {
  $params = _crowdriff_api_check_asset_filters($filter);

  if (($include = _crowdriff_api_check_asset_include($include))) {
    $params['include'] = $include;
  }

  return crowdriff_api_request('assets', $params);
}

/**
 * Retrieve a set of assets by tag.
 *
 * @param int $tag
 *   Tag ID.
 * @param array $filter
 *   Additional options to filter results by.
 * @param string $include
 *   Include the asset's associated tags and/or catalysts in the response body.
 *
 * @return array|false
 *   An array of asset objects, or FALSE if an error occurred.
 */
function crowriff_api_list_assets_by_tag($tag, array $filter = [], $include = NULL) {
  $filter['tag'] = $tag;
  return crowdriff_api_list_assets($filter, $include);
}

/**
 * Retrieve a set of assets by lens.
 *
 * @param int $lens
 *   Tag ID.
 * @param array $filter
 *   Additional options to filter results by.
 * @param string $include
 *   Include the asset's associated tags and/or catalysts in the response body.
 *
 * @return array|false
 *   An array of asset objects, or FALSE if an error occurred.
 */
function crowriff_api_list_assets_by_lens($lens, array $filter = [], $include = NULL) {
  $filter['lens'] = $lens;
  $params = _crowdriff_api_check_asset_filters($filter);

  if (($include = _crowdriff_api_check_asset_include($include))) {
    $params['include'] = $include;
  }

  return crowdriff_api_request('assets/lens', $params);
}

/**
 * Check the 'include' parameter for a valid value.
 *
 * @param string $value
 *   The include parameter value.
 *
 * @return string|null
 *   The include parameter value, or null if an invalid value was provided.
 *
 * @todo Support multiple values.
 *   This functionality is indicated in the API docs, but examples of the
 *   correct format are not provided.
 */
function _crowdriff_api_check_folder_include($value) {
  $allowedValues = [
    'tags',
  ];

  if (!in_array($value, $allowedValues)) {
    watchdog(
      'crowdriff',
      'Unknown folder include provided: %include',
      [
        '%include' => $value,
      ],
      WATCHDOG_NOTICE
    );
    return NULL;
  }

  return $value;
}

/**
 * Retrieve a single folder.
 *
 * @param string $id
 *   The folder ID.
 * @param string $include
 *   Include the folder's associated tags in the response body.
 *
 * @return object|false
 *   A folder object, or FALSE if an error occurred.
 */
function crowdriff_api_get_folder($id, $include = NULL) {
  $params = [];

  if (($include = _crowdriff_api_check_folder_include($include))) {
    $params['include'] = $include;
  }

  return crowdriff_api_request('folders/' . $id, $params);
}

/**
 * Retrieve a set of folders.
 *
 * @param string $include
 *   Include the folder's associated tags and/or catalysts in the response body.
 *
 * @return array|false
 *   An array of folder objects, or FALSE if an error occurred.
 */
function crowdriff_api_list_folders($include = NULL) {
  $params = [];

  if (($include = _crowdriff_api_check_folder_include($include))) {
    $params['include'] = $include;
  }

  return crowdriff_api_request('folders', $params);
}

/**
 * Retrieve a single tag.
 *
 * @param string $id
 *   The tag ID.
 *
 * @return object|false
 *   A tag object, or FALSE if an error occurred.
 */
function crowdriff_api_get_tag($id) {
  return crowdriff_api_request('tags/' . $id);
}

/**
 * Retrieve a set of tags.
 *
 * @return array|false
 *   An array of tag objects, or FALSE if an error occurred.
 */
function crowdriff_api_list_tags() {
  return crowdriff_api_request('tags');
}

/**
 * Check the 'include' parameter for a valid value.
 *
 * @param string $value
 *   The include parameter value.
 *
 * @return string|null
 *   The include parameter value, or null if an invalid value was provided.
 */
function _crowdriff_api_check_lens_include($value) {
  $allowedValues = [
    'tags',
  ];

  if (!in_array($value, $allowedValues)) {
    watchdog(
      'crowdriff',
      'Unknown lens include provided: %include',
      [
        '%include' => $value,
      ],
      WATCHDOG_NOTICE
    );
    return NULL;
  }

  return $value;
}

/**
 * Retrieve a single lens.
 *
 * The API doesn't provide an endpoint for a single lens, so retrieve all and
 * find the requested id within all results.
 *
 * @param string $id
 *   The lens ID.
 * @param string $include
 *   Include the folder's associated tags and/or catalysts in the response body.
 *
 * @return object|false
 *   A lens object, or FALSE if an error occurred.
 */
function crowdriff_api_get_lens($id, $include = NULL) {
  $allLenses = crowdriff_api_list_lenses($include);

  $foundLens = array_filter($allLenses, function ($lens) use ($id) {
    return $lens->id == $id;
  });

  if (empty($foundLens)) {
    return FALSE;
  }

  return reset($foundLens);
}

/**
 * Retrieve a set of lenses.
 *
 * @param string $include
 *   Include the folder's associated tags and/or catalysts in the response body.
 *
 * @return array|false
 *   An array of lense objects, or FALSE if an error occurred.
 */
function crowdriff_api_list_lenses($include = NULL) {
  $params = [];

  if (($include = _crowdriff_api_check_lens_include($include))) {
    $params['include'] = $include;
  }

  return crowdriff_api_request('lenses', $params);
}
