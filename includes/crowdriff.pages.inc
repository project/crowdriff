<?php

/**
 * @file
 * Page callbacks for CrowdRiff module.
 */

/**
 * Autocomplete callback for lenses.
 */
function crowdriff_lenses_autocomplete($string) {
  module_load_include('inc', 'crowdriff', 'includes/crowdriff.api');

  $allLenses = crowdriff_api_list_lenses();

  $foundLenses = array_filter($allLenses, function ($lens) use ($string) {
    return stripos($lens->label, $string) !== FALSE;
  });

  $matches = [];
  foreach ($foundLenses as $lens) {
    $matches[$lens->label . ' (' . $lens->id . ')'] = check_plain($lens->label);
  }
  asort($matches);
  array_slice($matches, 0, 10, TRUE);

  drupal_json_output($matches);
}
