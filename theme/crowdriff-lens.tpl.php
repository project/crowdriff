<?php

/**
 * @file
 * Template file for theme('crowdriff_lens').
 *
 * $id - The CrowdRiff Lens id.
 * $label - The CrowdRiff Lens label.
 * $hash - The CrowdRiff Lens hash.
 */
?>
<script id="cr__init-<?php print $hash ?>" src="https://embed.crowdriff.com/js/init?hash=<?php print $hash ?>" async></script>
