# CrowdRiff Visual Marketing Platform

## Requirements
 - PHP 5.5

## Submodules
The base module is enhanced by the following integration modules:

 - **CrowdRiff Media**  
   Provides integration with Media module.
 - **CrowdRiff Field**  
   Provides field types for CrowdRiff content.
