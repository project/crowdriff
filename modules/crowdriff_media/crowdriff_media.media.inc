<?php

/**
 * @file
 * Media module integration for the CrowdRiff Media module.
 */

/**
 * Implements hook_media_browser_plugin_info().
 */
function crowdriff_media_media_browser_plugin_info() {
  $info = [];

  $info['crowdriff_lens'] = [
    'title' => t('CrowdRiff Gallery'),
    'class' => 'MediaBrowserCrowdRiffLens',
  ];

  return $info;
}
