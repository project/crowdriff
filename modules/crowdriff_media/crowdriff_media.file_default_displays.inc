<?php

/**
 * @file
 * Default display configuration for the default file types.
 */

/**
 * Implements hook_file_default_displays().
 */
function crowdriff_media_file_default_displays() {
  $file_displays = [];

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'document__default__crowdriff_media_lens';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = [];
  $file_displays['document__default__crowdriff_media_lens'] = $file_display;

  return $file_displays;
}
