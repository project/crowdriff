# CrowdRiff Media Integration
Provides integration between CrowdRiff and
[Media](https://www.drupal.org/project/media) module.

## Requirements
 - PHP 5.5
 - [Media](https://www.drupal.org/project/media) module and dependencies
