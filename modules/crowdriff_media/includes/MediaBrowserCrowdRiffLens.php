<?php

/**
 * Media Browser plugin for CrowdRiff Lenses.
 */
class MediaBrowserCrowdRiffLens extends MediaBrowserPlugin {

  /**
   * {@inheritdoc}
   */
  public function access($account = NULL) {
    return crowdriff_media_access($account);
  }

  /**
   * {@inheritdoc}
   */
  public function view() {
    module_load_include('inc', 'file_entity', 'file_entity.pages');
    $build['form'] = drupal_get_form('crowdriff_media_add_lens_upload');
    return $build;
  }

}
