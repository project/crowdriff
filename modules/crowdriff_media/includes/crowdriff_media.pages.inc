<?php

/**
 * @file
 * Page callbacks for CrowdRiff Media module.
 */

/**
 * Title callback for gallery view page.
 */
function crowdriff_media_lenses_title($id) {
  if (($lens = _crowdriff_media_get_lens($id))) {
    return $lens->label;
  }

  return t('CrowdRiff Gallery');
}

/**
 * Page callback to view a gallery.
 */
function crowdriff_media_lenses_view($id) {
  // Title is not set on page by 'title callback' in hook_menu as expected.
  drupal_set_title(crowdriff_media_lenses_title($id));

  $lens = _crowdriff_media_get_lens($id);
  return [
    'crowdriff_lens' => [
      '#theme' => 'crowdriff_lens',
      '#id' => $lens->id,
      '#label' => $lens->label,
      '#hash' => $lens->hash,
    ],
  ];
}
