<?php

/**
 * @file
 * File formatters for CrowdRiff resources.
 */

/**
 * Implements hook_file_formatter_info().
 */
function crowdriff_media_file_formatter_info() {
  $formatters = [];

  $formatters['crowdriff_media_lens'] = [
    'label' => t('CrowdRiff Gallery'),
    'description' => t('CrowdRiff Gallery Formatter.'),
    'mime types' => ['application/x-crowdriff-lens'],
    'default settings' => [],
    'view callback' => 'crowdriff_media_file_formatter_lens_view',
  ];

  return $formatters;
}

/**
 * Implements hook_file_formatter_FORMATTER_view().
 */
function crowdriff_media_file_formatter_lens_view($file, $display, $langcode) {
  $scheme = file_uri_scheme($file->uri);

  if ($scheme == 'crowdriff') {
    $lens = _crowdriff_media_get_lens_from_uri($file->uri);

    // Gallery JS widget is not supported in WYSIWYG editor.
    if (!empty($file->override['wysiwyg'])) {
      $element = _crowdriff_media_file_formatter_lens_view_wysiwyg($lens);
    }
    else {
      $element = [
        '#theme' => 'crowdriff_lens',
        '#id' => $lens->id,
        '#label' => $lens->label,
        '#hash' => $lens->hash,
      ];
    }

    return $element;
  }
}

/**
 * Generate a gallery preview render array.
 *
 * @param object $lens
 *   The lens object to display.
 *
 * @return array
 *   The render array.
 *
 * @see crowdriff_media_file_formatter_lens_view()
 */
function _crowdriff_media_file_formatter_lens_view_wysiwyg($lens) {
  $element = [
    '#type' => 'markup',
    // Media replaces outer div with 'media-element' div, so need to wrap
    // in another layer to keep classes.
    '#prefix' => '<div><div class="crowdriff-lens-wysiwyg-preview">',
    '#suffix' => '</div></div>',
    'label' => [
      '#markup' => '<h2>' . $lens->label . '</h2>',
    ],
    'images' => [
      '#prefix' => '<div class="crowdriff-lens-wysiwyg-preview-images">',
      '#suffix' => '</div>',
    ],
    '#attached' => [
      'css' => [drupal_get_path('module', 'crowdriff_media') . '/css/crowdriff_media.wysiwyg.css'],
    ],
  ];

  module_load_include('inc', 'crowdriff', 'includes/crowdriff.api');
  $assets = crowriff_api_list_assets_by_lens($lens->id, ['size' => 8]);
  foreach ($assets as $asset) {
    $element['images'][] = [
      '#theme' => 'image',
      '#path' => $asset->thumbnail_embed,
    ];
  }

  return $element;
}
