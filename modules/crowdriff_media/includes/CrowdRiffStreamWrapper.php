<?php

/**
 * Stream Wrapper for CrowdRiff resources.
 *
 * Create an instance like this:
 * $resource = new CrowdRiffStreamWrapper('crowdriff://[resource-path]');
 */
class CrowdRiffStreamWrapper extends MediaReadOnlyStreamWrapper {

  /**
   * {@inheritdoc}
   */
  public static function getMimeType($uri, $mapping = NULL) {
    $path = substr($uri, 12);
    $pathFragments = explode('/', $path);

    switch ($pathFragments[0]) {
      case 'lenses':
        return 'application/x-crowdriff-lens';
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getExternalUrl() {
    $path = substr($this->getUri(), 12);
    $pathFragments = explode('/', $path);

    switch ($pathFragments[0]) {
      case 'lenses':
        return $this->base_url . '/crowdriff/media/lenses/' . $pathFragments[1];
    }

    return NULL;
  }

}
